﻿using System;
using System.Threading.Tasks;
using Blogs.DAL.Entities;
using Blogs.Models;
using Blogs.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Blogs.Controllers
{
    public class RecordController : Controller
    {
        private readonly IRecordService _recordService;
        private readonly UserManager<User> _userManager;

        public RecordController(IRecordService recordService, UserManager<User> userManager)
        {
            _recordService = recordService;
            _userManager = userManager;
        }

        public IActionResult Index(RecordIndexModel model)
        {
            try
            {
                var recordModels = _recordService.GetAllRecords(model);

                model.Records = recordModels;

                return View(model);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [Authorize]
        public IActionResult Create()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> CreateRecord(RecordCreateModel model)
        {
            try
            {
                User currentUser = await _userManager.GetUserAsync(User);

                 _recordService.CreateRecord(model, currentUser.Id);

                 return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
