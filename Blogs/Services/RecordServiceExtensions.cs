﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blogs.DAL.Entities;

namespace Blogs.Services
{
    public static class RecordServiceExtensions
    {
        public static IEnumerable<Record> BySearchKey(this IEnumerable<Record> records, string searchKey)
        {
            if (!string.IsNullOrWhiteSpace(searchKey))
                records = records.Where(r => r.Title.Contains(searchKey) || r.Content.Contains(searchKey));

            return records;
        }

        public static IEnumerable<Record> ByAuthorName(this IEnumerable<Record> records, string authorName)
        {
            if (!string.IsNullOrWhiteSpace(authorName))
                records = records.Where(r => r.Author.UserName.Contains(authorName));

            return records;
        }

        public static IEnumerable<Record> ByDateFrom(this IEnumerable<Record> records, DateTime? dateFrom)
        {
            if (dateFrom.HasValue)
                records = records.Where(r => r.DatePublished >= dateFrom.Value);

            return records;
        }

        public static IEnumerable<Record> ByDateTo(this IEnumerable<Record> records, DateTime? dateTo)
        {
            if (dateTo.HasValue)
                records = records.Where(r => r.DatePublished <= dateTo.Value);

            return records;
        }
    }
}
