﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Blogs.DAL.Entities
{
    public class User : IdentityUser<int>, IEntity
    {
        public ICollection<Record> Records { get; set; }
    }
}
